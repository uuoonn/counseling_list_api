package com.lyj.counselinglistapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AvailableTime {
    AM10("10:00-11:00","오전상담"),
    AM11("11:00-12:00","오전상담"),
    PM13("13:00-14:00","오후상담"),
    PM14("14:00-15:00","오후상담"),
    PM15("15:00-16:00","오후상담"),
    PM16("16:00-17:00","오후상담");

    private final String time;
    private final String isAm;


}
