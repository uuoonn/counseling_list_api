package com.lyj.counselinglistapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "counselingList App",
                description = "counselingList app api",
                version = "v1"))
@RequiredArgsConstructor
@Configuration
public class CounselingListConfig {
    @Bean
    public GroupedOpenApi counselingListOpenApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("상담정보 API v1")
                .pathsToMatch(paths)
                .build();
    }
}
