package com.lyj.counselinglistapi.repository;


import com.lyj.counselinglistapi.entity.CounselingList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CounselingListRepository extends JpaRepository<CounselingList, Long> {
}
