package com.lyj.counselinglistapi.service;

import com.lyj.counselinglistapi.entity.CounselingList;
import com.lyj.counselinglistapi.model.CounselingListItem;
import com.lyj.counselinglistapi.model.CounselingListRequest;
import com.lyj.counselinglistapi.model.CounselingListResponse;
import com.lyj.counselinglistapi.model.TimeChangeRequest;
import com.lyj.counselinglistapi.repository.CounselingListRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor

public class CounselingListService {
    private final CounselingListRepository counselingListRepository;

    public void setCounselingList (CounselingListRequest request){
        CounselingList addData = new CounselingList();
        addData.setName(request.getName());
        addData.setGender(request.getGender());
        addData.setBirthDate(request.getBirthDate());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setAvailableTime(request.getAvailableTime());

        counselingListRepository.save(addData);
    }

    public List<CounselingListItem> getItems() {
        List<CounselingList> originData = counselingListRepository.findAll();

        List<CounselingListItem> result = new LinkedList<>();

        for(CounselingList counselingList : originData) {
            CounselingListItem addItem = new CounselingListItem();
            addItem.setId(counselingList.getId());
            addItem.setName(counselingList.getName());
            addItem.setBirthDate(counselingList.getBirthDate());
            addItem.setAvailableTimeIsAm(counselingList.getAvailableTime().getIsAm());

            result.add(addItem);
        }

        return result;
    }

    public CounselingListResponse getDetailInfo(Long id) {
        CounselingList originData = counselingListRepository.findById(id).orElseThrow();

            CounselingListResponse response = new CounselingListResponse();
            response.setId(originData.getId());
            response.setName(originData.getName());
            response.setGenderName(originData.getGender() ? "여자" : "남자");
            response.setBirthDate(originData.getBirthDate());
            response.setPhoneNumber(originData.getPhoneNumber());
            response.setAvailableTimeTime(originData.getAvailableTime().getTime());
            response.setAvailableTimeIsAm(originData.getAvailableTime().getIsAm());

            return response;
        }


    public void putCounselingList(long id, TimeChangeRequest request){
        CounselingList originData = counselingListRepository.findById(id).orElseThrow();
        originData.setAvailableTime(request.getAvailableTime());

        counselingListRepository.save(originData);
        }

    public void delCounselingList(long id){
        counselingListRepository.deleteById(id);
    }


    }






