package com.lyj.counselinglistapi.model;

import com.lyj.counselinglistapi.enums.AvailableTime;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class TimeChangeRequest {
    private AvailableTime availableTime;
}
