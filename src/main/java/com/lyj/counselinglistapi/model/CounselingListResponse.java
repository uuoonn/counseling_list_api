package com.lyj.counselinglistapi.model;

import com.lyj.counselinglistapi.enums.AvailableTime;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CounselingListResponse {
    private Long id;
    private String name;
    private String genderName;
    private LocalDate birthDate;
    private String phoneNumber;
    private String availableTimeTime;
    private String availableTimeIsAm;

}
