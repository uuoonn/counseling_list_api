package com.lyj.counselinglistapi.model;

import com.lyj.counselinglistapi.enums.AvailableTime;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter

public class CounselingListItem {
    private Long id;
    private String name;
    private LocalDate birthDate;
    private String availableTimeIsAm;

}
