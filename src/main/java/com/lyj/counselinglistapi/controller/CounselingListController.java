package com.lyj.counselinglistapi.controller;

import com.lyj.counselinglistapi.model.CounselingListItem;
import com.lyj.counselinglistapi.model.CounselingListRequest;
import com.lyj.counselinglistapi.model.CounselingListResponse;
import com.lyj.counselinglistapi.model.TimeChangeRequest;
import com.lyj.counselinglistapi.repository.CounselingListRepository;
import com.lyj.counselinglistapi.service.CounselingListService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/counseling")

public class CounselingListController {
    private final CounselingListService counselingListService;

    @PostMapping("/creat")
    public String setCounselingList (@RequestBody CounselingListRequest request){
        counselingListService.setCounselingList(request);

        return "ok";

    }

    @GetMapping("/list")
    public List<CounselingListItem> getItems() {
        return counselingListService.getItems();
    }

    @GetMapping("/detail/{id}")
    public CounselingListResponse getDetailInfo(@PathVariable Long id){
        return counselingListService.getDetailInfo(id);
    }

    @PutMapping("/edit-time/{id}")
    public String putCounselingList(@PathVariable long id, @RequestBody TimeChangeRequest request){
        counselingListService.putCounselingList(id,request);

        return "시간 변경 완료";
    }

    @DeleteMapping("/del/{id}")
    public String delCounselingList(@PathVariable long id){
        counselingListService.delCounselingList(id);

        return "삭제완료";

    }
}
